//importa o JSON com a relação de Agentes
const agents = require('../../../env/dev/mock/agents.json');

//Acessa o metodo get exportado
exports.get = (req, res, next) => {
   let id = req.query.id;
   let type = req.query.type;
   let status = req.query.status;
   let cpf = req.query.cpf;
   let cnpj = req.query.cnpj;
   let typePeople = req.query.typePeople;
   let uf = req.query.uf;
   let email = req.query.email;

   res.status(200).send(agents.filter((column) =>
   (
      (id         == null || id         == 'all' || column.AGN_IN_CODIGO       == id        )
      &&
      (type       == null || type       == 'all' || column.AGN_TAU_ST_CODIGO   == type      )
      &&
      (status     == null || status     == 'all' || column.AGN_CH_STATUS       == status    )
      && 
      (typePeople == null || typePeople == 'all' || column.AGN_CH_TIPOPESSOAFJ == typePeople)
      &&
      (cpf        == null || cpf        == 'all' || column.AGN_ST_CPF          == cpf       )
      && 
      (cnpj       == null || cnpj       == 'all' || column.AGN_ST_CGC          == cnpj      )
      &&
      (uf         == null || uf         == 'all' || column.UF_ST_SIGLA         == uf        )
      &&
      (email      == null || email      == 'all' || column.AGN_ST_EMAIL        == email     )
   ))
   );

};

exports.post = (req, res, next) => {
   let id = req.query.id;
   if (id) {
      res.status(201).send(`POST com Param -> id=${id}`);
   } else {
      res.status(201).send("POST sem Param");
   }
};

exports.put = (req, res, next) => {
   let id = req.query.id;
   if (id) {
      res.status(201).send(`PUT com Param -> id=${id}`);
   } else {
      res.status(201).send("PUT sem Param");
   }
};

exports.delete = (req, res, next) => {
   let id = req.query.id;
   if (id) {
      res.status(200).send(`DELETE com Param -> id=${id}`);
   } else {
      res.status(200).send("DELETE sem Param");
   }
};

exports.get = (req, res, next) => {
   let id = req.query.id;
   console.log('id', id);
   if (id) {
      res.status(200).send(agents[id]);
   } else {
      res.status(200).send(agents);
   }
};
