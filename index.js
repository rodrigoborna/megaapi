//Framework, fornece metodos HTTP como get, post, delete.
const express = require('express');

//Controla as origens das requisições.
const cors = require('cors');

//Inicia o servidor
const app = express()

//Determina o tipo de dados que será recebido
app.use(express.json())

//Inclui o cors na aplicação
app.use(cors());

//importa o modulo de checagem de status
const routes = require('./src/routes/healthcheck')

//define a rota e fornece o modulo de checagem de status
app.use('/API/healthcheck', routes)

//passa a referencia do app para o modulo index
require('./src/routes/index')(app)

//define a porta que receberá as chamadas
app.listen(3000, () => console.log("API OK!"))