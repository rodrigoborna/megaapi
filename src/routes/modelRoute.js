//importa os metodos HTTP que tratam as requisições de Agentes
const modelController = require('./api/model');

//exporta as rotas dos metodos HTTP
module.exports = (app) => {
   app.post('/api/model', modelController.post);
   app.put('/api/model', modelController.put);
   app.delete('/api/model', modelController.delete);
   app.get('/api/model', modelController.get);
}
